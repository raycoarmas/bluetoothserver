package com.example.bluetoothserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	public BluetoothAdapter mBluetoothAdapter;
	private BluetoothServerSocket mmServerSocket;
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private TextView text;
	public InputStream input;
	public OutputStream out;
	private Button stop;
	private Button start;
	public BluetoothSocket socket;
	public Boolean fin=false;
	private int contador;
	public int x=0;
	private int[] aux;
	public int[] [] matrizMoc = {{0,900,0,1}, 
			{10,2000,55,14}, 
			{15,1500,80,8},
			{22,2158,30,10},
			{35,1350,40,7},//km/h , rpm, pos acelerador %, consumo actual en L/100km
			{42,2300,60,9},//01 0d, 01 0c, 01 11,             01 5e                    el 01 0e solo es 1 o 0
			{55,1300,50,6},
			{30,900,0,1},
			{35,2054,80,10},
			{42,1157,60,6},
			{50,2315,70,7},
			{60,1100,70,3},
			{75,1305,78,4},
			{82,1458,77,5},
			{90,1640,77,6},
			{103,1850,70,8},
			{111,2005,70,9},
			{120,2200,70,11}};
	//diesel
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		text=(TextView)findViewById(R.id.textView1);
		stop = (Button)findViewById(R.id.button1);
		start= (Button)findViewById(R.id.button2);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AcceptThread();
				while(mmServerSocket==null);
				run();

			}
		});

		stop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				try {
					fin=true;
					wait(10000);
					socket.close();
					mmServerSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});



	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}



	

	public void AcceptThread() {
		// Use a temporary object that is later assigned to mmServerSocket,
		// because mmServerSocket is final
		BluetoothServerSocket tmp = null;
		try {
			// MY_UUID is the app's UUID string, also used by the client code
			tmp = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("My_APP", MY_UUID);
		} catch (IOException e) { }
		mmServerSocket = tmp;
	}

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			String aux = bundle.getString("key");
			text.setText(aux);
			//new HiloEscritura().send();
		}
	};


	public void run() {
		// Keep listening until exception occurs or a socket is returned
		while (true) {
			try {
				socket = mmServerSocket.accept();

			} catch (IOException e) {
				break;
			}
			// If a connection was accepted
			if (socket != null) {
				new HiloEscritura().start();
				// Do work to manage the connection (in a separate thread)
				//manageConnectedSocket(socket);
				/*try {
					mmServerSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				break;
			}
		}
	}
	public void moc(){
			aux = new int[matrizMoc[x].length];
			for(int y=0;y < matrizMoc[x].length;y++){
				aux [y] =matrizMoc[x][y];
			}
	}
	public class HiloEscritura  extends Thread{
		public void run(){
			moc();
			
			contador=0;
			byte[] buffer = new byte[5];
			int bytes;
			Bundle bundle = new Bundle();
			try {
			input = socket.getInputStream();
			
				while(true){
					String readMessage="";
					
					
					bytes = input.read(buffer);
					readMessage = new String(buffer, 0, bytes);// km/h , rpm, pos acelerador %, consumo actual en L/100km
					if (readMessage.equals("01 0d")){//speed
						send(readMessage+" "+aux[0]);
						contador++;
					}else if (readMessage.equals("010c")){//rpm
						send(readMessage+aux[1]);
						contador++;
					}else if(readMessage.equals("01 11")){//postAcelerador
						send(readMessage+" "+aux[2]);
						contador++;
					}else if(readMessage.equals("01 5e")){//Consumo
						send(readMessage+" "+aux[3]);
						contador++;
					}else if (readMessage.equals("01 0e")){//powerNetwork
						if (fin){
							send(readMessage+" "+1);
						}else{
							send(readMessage+" "+0);
						}
					}
					if(contador==1){
						if (x < matrizMoc.length){
							x++;
						}
						moc();
						contador=0;
					}
					bundle.putString("key", readMessage);
					Message msg = new Message();
					msg.setData(bundle);
					mHandler.sendMessage(msg);
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		public void send(String msg){
			try {
				out = socket.getOutputStream();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (out!=null){
				try {

					//for (int i=0; i< 100;i++)
					out.write((msg).getBytes());

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


		}

	}
}